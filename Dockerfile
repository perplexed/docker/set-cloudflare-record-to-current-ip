FROM alpine:3.11

RUN apk add --no-cache bash \
                       curl \
                       jq \
                       bind-tools
ADD update-record.sh /
ENTRYPOINT ["/bin/bash","./update-record.sh"]
